import "./style.scss"

import MapTracer from "./components/MapWithCluster"
import React from "react"

export default () => {
  const onClickMarker = markerInfo => {
    //markerInfo will be passed from markerInfo arg
  }
  return <MapTracer onClickMarker={onClickMarker} />
}
