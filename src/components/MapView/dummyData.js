import { useCallback, useState } from "react"

import faker from "faker"

const makeDummyCases = () =>
  new Array(100).fill({}).map((_case, index) => ({
    id: faker.random.uuid(),
    sex: faker.helpers.randomize(["Male", "Female"]),
    location: {
      lat: parseFloat(faker.address.latitude()),
      lng: parseFloat(faker.address.longitude()),
    },
    age: faker.random.number({ min: 5, max: 80 }),
    icon: faker.internet.avatar(),
    statement: faker.lorem.paragraph(),
    caseNumber: index,
    countryCode: faker.address.countryCode(),
  }))

export const useFakeCases = (props = {}) => {
  const [cases, setCases] = useState([])

  const getCases = useCallback(async () => {
    await new Promise(resolve => {
      setTimeout(() => {
        resolve(makeDummyCases())
      }, 1000)
    }).then(data => {
      if (props.usePoints) {
        const newPoints = data.map(info => ({
          type: "Feature",
          properties: { cluster: false, ...info },
          geometry: {
            type: "Point",
            coordinates: [info.location.lng, info.location.lat],
          },
        }))
        setCases(newPoints)
      } else {
        setCases(data)
      }
    })
  }, [])

  return [cases, getCases]
}
