import React from "react"
import { withPrefix } from "gatsby"

export default ({ $hover, selected, ...info }) => (
  <div
    className={`medkitIcon${$hover ? " marker-hovered" : ""}${
      selected ? " marker-selected" : ""
    } markerBounceIn`}
    style={{ backgroundImage: `url(${withPrefix("images/testKit.png")})` }}
  ></div>
)
