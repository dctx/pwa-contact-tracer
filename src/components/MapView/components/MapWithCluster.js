import React, { useCallback, useEffect, useRef, useState } from "react"

import ClusterIcon from "./ClusterIcon"
import GoogleMapReact from "google-map-react"
import MarkerIcon from "./PersonIcon"
import MedKitIcon from "./MedKitIcon"
import { useFakeCases } from "../dummyData.js"
import useSuperCluster from "use-supercluster"

const createMapOptions = () => ({
  fullscreenControl: false,
  minZoom: 0,
})

const mapOptions = {
  center: {
    lat: 14.58226,
    lng: 120.9748,
  },
  defaultZoom: 3,
  bootstrapURLKeys: { key: "" },
  yesIWantToUseGoogleMapApiInternals: true,
  options: createMapOptions,
}

export default ({ onClickMarker, clusterPrefix = "Med Kit" }) => {
  const [points, getCases] = useFakeCases({ usePoints: true }),
    [selectedMarkerId, setSelectedMarker] = useState(null),
    mapRef = useRef(),
    [zoom, setZoom] = useState(10),
    [bounds, setBounds] = useState([]),
    { clusters } = useSuperCluster({
      points,
      bounds,
      zoom,
      options: { radius: 75, maxZoom: 20 },
    })

  const onChildClick = useCallback((caseId, info) => {
    if (!info.pointCount && !!onClickMarker) {
      onClickMarker(info)
    }
    mapRef.current.panTo({
      lat: info.lat,
      lng: info.lng,
    })
    setSelectedMarker(caseId)
  }, [])

  const zoomIn = () => {
    const { zoom } = mapRef.current
    mapRef.current.setZoom(zoom + 1)
  }

  useEffect(() => {
    getCases()
  }, [])

  return (
    <div className="mapContainer">
      <GoogleMapReact
        {...mapOptions}
        onChildClick={onChildClick}
        onGoogleApiLoaded={({ map }) => {
          mapRef.current = map
        }}
        onChange={({ zoom, bounds }) => {
          setZoom(zoom)
          setBounds([
            bounds.nw.lng,
            bounds.se.lat,
            bounds.se.lng,
            bounds.nw.lat,
          ])
        }}
      >
        {clusters.map(cluster => {
          const [longitude, latitude] = cluster.geometry.coordinates
          const {
            cluster: isCluster,
            point_count: pointCount,
            location: markerLocation,
            ...markerInfo
          } = cluster.properties

          if (isCluster) {
            return (
              <ClusterIcon
                key={cluster.id}
                pointCount={pointCount}
                lat={latitude}
                lng={longitude}
                prefix={clusterPrefix}
                onClick={zoomIn}
              />
            )
          }
          return (
            <MedKitIcon
              key={markerInfo.id}
              {...markerLocation}
              {...markerInfo}
              selected={selectedMarkerId === markerInfo.id}
            />
          )
        })}
      </GoogleMapReact>
    </div>
  )
}
