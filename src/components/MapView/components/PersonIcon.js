import React from "react"

export default ({ $hover, selected, ...info }) => (
  <div
    className={`markerIcon${$hover ? " marker-hovered" : ""}${
      selected ? " marker-selected" : ""
    } markerBounceIn`}
    style={{ backgroundImage: `url(${info.icon})` }}
  >
    <p>
      {info.countryCode} {info.caseNumber}
    </p>
  </div>
)
