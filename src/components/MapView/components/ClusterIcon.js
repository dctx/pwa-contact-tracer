import React from "react"

export default ({ $hover, pointCount, prefix, color, onClick }) => (
  <div
    className={`clusterIcon${$hover ? " cluster-hovered" : ""} markerBounceIn`}
    style={{ backgroundColor: color || "#ef5350" }}
    onClick={onClick}
  >
    <p>
      {pointCount} {prefix}
    </p>
  </div>
)
