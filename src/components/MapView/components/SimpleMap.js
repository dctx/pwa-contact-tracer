import React, { useEffect, useState } from "react"

import GoogleMapReact from "google-map-react"
import MarkerIcon from "./PersonIcon"
import { useFakeCases } from "../dummyData.js"

const createMapOptions = () => ({
  fullscreenControl: false,
})

const defaultMapOptins = {
  center: {
    lat: 14.58226,
    lng: 120.9748,
  },
  defaultZoom: 10,
  bootstrapURLKeys: { key: "AIzaSyDldwRBF-TWYm1PJ3nXABYOqrVbhbXUZug" },
  yesIWantToUseGoogleMapApiInternals: true,
  options: createMapOptions,
}

export default () => {
  const [cases, getCases] = useFakeCases(),
    [selectedMarkerId, setSelectedMarker] = useState(null)

  useEffect(() => {
    getCases()
  }, [])

  return (
    <div className="mapContainer">
      <GoogleMapReact
        {...defaultMapOptins}
        onChildClick={caseId => setSelectedMarker(caseId)}
      >
        {cases.map(({ location, ...info }) => {
          return (
            <MarkerIcon
              key={info.id}
              {...location}
              {...info}
              selected={selectedMarkerId === info.id}
            />
          )
        })}
      </GoogleMapReact>
    </div>
  )
}
