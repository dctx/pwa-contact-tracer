import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import { withPrefix } from "gatsby"

const useStyles = makeStyles(theme => {
  return {
    dctx: {
      width: "100%",
      padding: "20px 0px",
      backgroundColor: "white",
      textAlign: "center",
      position: "absolute",
      bottom: 0,
      left: 0,
      "& p": {
        color: "#5e35b1",
        letterSpacing: "3px",
        fontSize: "8px",
      },
      "& img": {
        margin: `0 ${theme.spacing(2)}px`,
        verticalAlign: "middle",
      },
    },
  }
})

const InfoFooter = () => {
  const classes = useStyles()

  return (
    <footer className={classes.dctx}>
      <Typography variant="body1" gutterBottom>
        POWERED BY
      </Typography>
      <img alt="" src={withPrefix("images/dctx_logo.png")}></img>
      <img alt="" src={withPrefix("images/devcon.png")}></img>
    </footer>
  )
}

export default InfoFooter
