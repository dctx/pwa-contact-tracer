import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { withPrefix } from "gatsby"
import MainContainer from "./mainContainer"
import { ThemeProvider } from "@material-ui/core/styles"
import ThemeGlobal from "../../utilities/themeGlobal"
import CssBaseline from "@material-ui/core/CssBaseline"
import NoSsr from "@material-ui/core/NoSsr"
import SEO from "./seo"
import Header from "./header"

import "../../scss/index.scss"

const Layout = ({ children, pageTitle, isPlain }) => {
  return (
    <>
      <SEO title={pageTitle} />
      <NoSsr>
        <ThemeProvider theme={ThemeGlobal}>
          <CssBaseline />
          {!isPlain && <Header pageTitle={pageTitle} />}
          <MainContainer>{children}</MainContainer>
        </ThemeProvider>
        <Helmet>
          <script
            async
            defer
            src={withPrefix("js/compressed.js")}
            type="text/javascript"
          />
        </Helmet>
      </NoSsr>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  pageTitle: PropTypes.string,
  isPlain: PropTypes.bool,
}

Layout.defaultProps = {
  isPlain: false,
  pageTitle: ``,
}

export default Layout
