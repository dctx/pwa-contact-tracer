/* Easy setup of modal routing context,
    converted ReactContext to hook `useContext`
  */
import { Link, ModalRoutingContext } from "gatsby-plugin-modal-routing"

import React from "react"

export const useModal = () => {
  const { modal, closeTo } = React.useContext(ModalRoutingContext)

  React.useEffect(() => {
    const contentEl = document.getElementsByClassName("ReactModal__Content")[0]
    let addTimeout

    if (!!contentEl) {
      // setTimeout is used for a syncrhonous adding classname "customModal"
      // Becuase the modal plugin uses className on show of modal that is not customizeable

      addTimeout = setTimeout(() => {
        contentEl.className += " customModal"
      }, 100)
    }
    return () => clearTimeout(addTimeout)
  }, [])
  // element.classList.add("myModalStyle")
  return [modal, closeTo]
}

/* Link component can be use in both page and modal.
    NOTE: If your page using linking to a Modal, you can use Link component from here instead from gatsby,
          - Link is built on top of React Router's Link so you can pass a valid props (e.g. replace)

    NOTE 2: When using useModal in development, a warning occuring. Please disregard it due to unhandled gatsby unknown error in hot reload module.
*/
export { Link }
