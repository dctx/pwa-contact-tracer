import "./style.scss"

import Layout from "@components/skeleton/layout"
import PropTypes from "prop-types"
import React from "react"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles(theme => {
  return {
    texts: {
      color: theme.palette.common.white,
      "& h1": {
        fontWeight: 600,
        textShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        fontSize: 40,
        lineHeight: 3,
        paddingTop: "10%",
      },
      "& h2": {
        lineHeight: 1.3,
      },
    },
  }
})
/**
 * https://codepen.io/marianarlt/pen/NxWXXd
 */
const DefaultPage = ({ pageTitle }) => {
  const classes = useStyles()
  return (
    <>
      <Layout pageTitle={pageTitle}>
        <div className={classes.texts}>
          <Typography component="h1">&lt; building /&gt;</Typography>
          <Typography component="h2">Working day and night</Typography>
          <Typography component="h2">
            for this <b>{pageTitle}</b> page!
          </Typography>
        </div>
        <div id="outerCraneContainer">
          <div className="buildings">
            <div />
            <div className={1} />
            <div className={2} />
            <div className={3} />
            <div className={4} />
          </div>
          <div className="crane craneThree">
            <div className="line lineOne" />
            <div className="line lineTwo" />
            <div className="line lineThree" />
            <div className="stand" />
            <div className="weight" />
            <div className="cabin" />
            <div className="arm" />
          </div>
          <div className="crane craneTwo">
            <div className="line lineOne" />
            <div className="line lineTwo" />
            <div className="line lineThree" />
            <div className="stand" />
            <div className="weight" />
            <div className="cabin" />
            <div className="arm" />
          </div>
          <div className="crane craneOne">
            <div className="line lineOne" />
            <div className="line lineTwo" />
            <div className="line lineThree" />
            <div className="stand" />
            <div className="weight" />
            <div className="cabin" />
            <div className="arm" />
          </div>
        </div>
      </Layout>
    </>
  )
}
DefaultPage.propTypes = {
  pageTitle: PropTypes.string,
}

export default DefaultPage
