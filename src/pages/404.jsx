import React from "react"
import Layout from "../components/skeleton/layout"

const NotFoundPage = () => (
  <Layout pageTitle="404: Not found">
    <div style={{ color: "white" }}>
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </div>
  </Layout>
)

export default NotFoundPage
