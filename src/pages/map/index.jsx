import Layout from "@components/skeleton/layout"
import MapView from "@components/MapView"
import React from "react"
import mapViewStyle from "./__style"

const Map = () => {
  const classes = mapViewStyle()
  return (
    <Layout pageTitle="Location">
      <div className={classes.main}>
        <div className={classes.mapView}>
          <MapView />
        </div>
      </div>
    </Layout>
  )
}

export default Map
