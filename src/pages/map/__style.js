import { makeStyles } from "@material-ui/core/styles"

const LandingStyle = makeStyles(theme => {
  return {
    main: {
      width: "100vw",
      backgroundColor: theme.palette.common.white,
    },
    mapView: {
    },
  }
})

export default LandingStyle
