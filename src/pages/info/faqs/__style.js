import { makeStyles } from "@material-ui/core/styles"

const LandingStyle = makeStyles(theme => {
  return {
    root: {
      textAlign: "center",
      marginTop: theme.spacing(3),
    },
    container: {
      textAlign: "center",
    },
    main: {
      position: "relative",
      width: "100vw",
      minHeight: "100vh",
      backgroundColor: theme.palette.common.white,
      paddingTop: "calc(10% + 31px)",
      padding: "0px 21px",
    },
    heading: {
      fontWeight: "700",
      color: "#5E35B1",
      textAlign: "initial",
    },
    faqsList: {
      marginBottom: 31,
    },
    expansionPanel: {
      margin: 0,
      boxShadow: "none",
      borderBottom: `1px solid ${theme.palette.grey[400]}`,
    },
    expansionSummary: {
      padding: 0,
    },
    expansionDetails: {
      padding: 0,
      paddingBottom: 19,
    },
  }
})

export default LandingStyle
