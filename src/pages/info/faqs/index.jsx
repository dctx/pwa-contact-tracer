import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
} from "@material-ui/core"
import React, { useState } from "react"

import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import Footer from "@components/infoFooter"
import Layout from "@components/skeleton/layout"
import Typography from "@material-ui/core/Typography"
import faqStyle from "./__style"

const faqs = [
  {
    id: 1,
    question: "How long is the approval process?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
  {
    id: 2,
    question: "How will I know the results of my application?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
  {
    id: 3,
    question: "Where can I use RapidPass?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
  {
    id: 4,
    question: "What do I have to do if I need to pass multiple checkpoints?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
  {
    id: 5,
    question: "Can I apply for RapidPass that covers for more than 1 day?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
  {
    id: 6,
    question: "What happens when I used an expired RapidPass?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
  {
    id: 7,
    question: "Can my family members use my RapidPass?",
    answer:
      "Approval process may take from a few minutes up to 24 hours upon submission of application. Results will be notified via text and email.",
  },
]

const FAQPage = () => {
  const classes = faqStyle(),
    [selected, setSelected] = useState(null)

  return (
    <>
      <Layout pageTitle="FAQ's">
        <div className={classes.main}>
          <div className={classes.faqsList}>
            {faqs.map(faq => (
              <ExpansionPanel
                square
                className={classes.expansionPanel}
                key={faq.id}
                expanded={selected === faq.id}
                onChange={() =>
                  setSelected(selected === faq.id ? null : faq.id)
                }
              >
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  className={classes.expansionSummary}
                >
                  <Typography variant="subtitle1" className={classes.heading}>
                    {faq.question}
                  </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.expansionDetails}>
                  <Typography variant="subtitle1">{faq.answer}</Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            ))}
          </div>
        </div>
        <Footer />
      </Layout>
    </>
  )
}

export default FAQPage
