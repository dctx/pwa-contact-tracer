import { makeStyles } from "@material-ui/core/styles"

const AboutUsStyle = makeStyles(theme => {
  return {
    main: {
      width: "100%",
      height: "100vh",
      backgroundColor: theme.palette.common.white,
      padding: "0px 40px",
      paddingTop: "calc(10% + 47px)",
    },
    logo: {
      width: 240,
      height: 65,
      margin: "0 auto",
      marginBottom: 35,
    },
    desc: {
      textAlign: "initial",
      maxWidth: 450,
      margin: "0 auto",
    },
  }
})

export default AboutUsStyle
