import AboutUsStyle from "./__style"
import Footer from "@components/infoFooter"
import Layout from "@components/skeleton/layout"
import React from "react"
import Typography from "@material-ui/core/Typography"
import { withPrefix } from "gatsby"

const index = () => {
  const classes = AboutUsStyle()
  return (
    <>
      <Layout pageTitle="About us">
        <div className={classes.main}>
          <img
            className={classes.logo}
            src={withPrefix("images/inlineLogo.png")}
          />
          <Typography className={classes.desc} variant="body1">
            TraceCovid is powered by DCTX, a team quarantined, yet passionate
            designers-volunteers, under DevCon.ph COVID-19 Technology
            Initiative. We exist to empathize, define, ideate and design
            solutions to help solve pressing COVID-19 pandemic problems.
          </Typography>
          <Footer />
        </div>
      </Layout>
    </>
  )
}

export default index
