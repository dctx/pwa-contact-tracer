import Footer from "@components/infoFooter"
import Layout from "@components/skeleton/layout"
import React from "react"
import Typography from "@material-ui/core/Typography"
import informations from "../../../content/privacy.json"
import privacyStyle from "./__style"

const Privacy = () => {
  const classes = privacyStyle()

  return (
    <>
      <Layout pageTitle="Privacy policy">
        <div className={classes.main}>
          {informations.map(({ title, description, enums }, i) => {
            return (
              <React.Fragment key={i}>
                {title && (
                  <Typography variant="subtitle1" className={classes.title}>
                    {title}
                  </Typography>
                )}
                {description && (
                  <Typography variant="body1" className={classes.description}>
                    {description}
                  </Typography>
                )}
                {!!enums &&
                  enums.map((info, i) => (
                    <Typography
                      key={i}
                      variant="body1"
                      className={classes.description}
                    >
                      {`${i + 1}. ${info}`}
                    </Typography>
                  ))}
              </React.Fragment>
            )
          })}
        </div>
        <Footer />
      </Layout>
    </>
  )
}

export default Privacy
