import { makeStyles } from "@material-ui/core/styles"

const privacyStyle = makeStyles(theme => {
  return {
    main: {
      width: "100%",
      minHeight: "100%",
      padding: "20px 38px",
      paddingTop: "calc(10% + 31px)",
      paddingBottom: "80px",
      backgroundColor: theme.palette.common.white,
    },
    title: {
      fontWeight: "bold",
      textAlign: "initial",
      fontFamily: "Work Sans, sans-serif",
    },
    description: {
      marginBottom: 25,
      textAlign: "initial",
      fontFamily: "Work Sans, sans-serif",
    },
  }
})

export default privacyStyle
