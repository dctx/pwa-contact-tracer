import { makeStyles } from "@material-ui/core/styles"

const ContactUsStyle = makeStyles(theme => {
  return {
    main: {
      width: "100vw",
      minHeight: "100vh",
      backgroundColor: theme.palette.common.white,
      padding: "0px 40px",
      paddingTop: "calc(10% + 47px)",
    },
    logo: {
      width: 240,
      height: 65,
      margin: "0 auto",
      marginBottom: 35,
    },
    desc: {
      textAlign: "initial",
      maxWidth: 450,
      margin: "0 auto",
    },
    link: {
      display: "flex",
      alignItems: "center",
      fontWeight: "700",
      fontFamily: "Work Sans, sans-serif",
      marginBottom: 15,
      "& > span": {
        marginRight: 14,
        paddingTop: 5,
        "& > svg": {
          fill: "#5e35b1",
        },
      },
    },
    linkList: {
      maxWidth: 450,
      margin: "0 auto",
      marginTop: 30,
    },
  }
})

export default ContactUsStyle
