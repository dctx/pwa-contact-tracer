import { Facebook, MailOutline, Twitter } from "@material-ui/icons"
import { Link, Typography } from "@material-ui/core"
import { isAndroid, isIOS, isMobile } from "react-device-detect"

import Footer from "@components/infoFooter"
import Layout from "@components/skeleton/layout"
import React from "react"
import contactUsStyle from "./__style"
import { withPrefix } from "gatsby"

const Contact = () => {
  const classes = contactUsStyle()
  return (
    <>
      <Layout pageTitle="Contact us">
        <div className={classes.main}>
          <img
            className={classes.logo}
            src={withPrefix("images/inlineLogo.png")}
          />
          <Typography className={classes.desc} variant="body1">
            If you have any questions, remarks or suggestions, feel free to
            contact us using the channels below. We will get back to you as soon
            as possible.
          </Typography>

          <div className={classes.linkList}>
            <Link
              className={classes.link}
              color="inherit"
              href="mailto:info@rapidpass.com"
              targett="_blank"
              rel="alternate"
            >
              <span>
                <MailOutline />
              </span>{" "}
              info@rapidpass.com
            </Link>
            <Link
              className={classes.link}
              color="inherit"
              target="_blank"
              rel="alternate"
              href={
                isIOS
                  ? "fb://page/?id=151052508584"
                  : isAndroid
                  ? "fb://page/151052508584"
                  : "https://www.facebook.com/groups/devconph"
              }
            >
              <span>
                <Facebook />
              </span>{" "}
              facebook.com/rapidpassph
            </Link>
            <Link
              className={classes.link}
              color="inherit"
              href={
                isMobile ? "twitter://devconph" : "https://twitter.com/devconph"
              }
            >
              <span>
                <Twitter />
              </span>{" "}
              twitter.com/rapidpassph
            </Link>
          </div>
        </div>
      </Layout>
      <Footer />
    </>
  )
}

export default Contact
