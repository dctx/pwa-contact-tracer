import { makeStyles } from "@material-ui/core/styles"

const LandingStyle = makeStyles(theme => {
  return {
    root: {
      textAlign: "center",
      marginTop: theme.spacing(3),
    },
    container: {
      textAlign: "center",
    },
    main: {
      height: 419,
      position: "absolute",
      width: "100vw",
      marginTop: "10%",
    },
    desc: {
      textAlign: "center",
      color: theme.palette.common.white,
      width: 238,
      margin: "0 auto",
      "& h1": {
        fontWeight: "bold",
        fontSize: 32,
        lineHeight: 0.7,
      },
      "& h2": {
        lineHeight: 2,
        fontStyle: "normal",
        fontWeight: 300,
        fontSize: "24px",
        opacity: 0.9,
      },
    },
    buttons: {
      textDecoration: "none",
      margin: "10px",
      color: "black",
      "& button": {
        backgroundColor: "white",
        borderRadius: "8px",
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: 150,
        height: 150,
        padding: 10,
        paddingTop: 0,
        "& span.MuiButton-label": {
          textAlign: "center",
          color: "black",
          "& span": {
            fontWeight: "600",
          },
          "& img": {
            margin: "0 auto",
          },
        },
        "& *": {
          display: "block",
        },
      },
    },
    typography: {
      color: "white",
    },
    navigationButtons: {
      padding: theme.spacing(2),
      "& a": {
        margin: "0 auto",
        width: 315,
        display: "block",
        borderRadius: 0,
        borderBottom: "solid 1px white",
        textAlign: "start",
        opacity: 0.8,
        textDecoration: "none",
        padding: "10px 0",
        color: theme.palette.common.white,
        "& span": {
          color: theme.palette.common.white,
          verticalAlign: "middle",
          paddingRight: theme.spacing(2),
        },
        "&:hover": {
          opacity: 1,
        },
      },
      "& .MuiButton-label": {
        display: "inline-block",
        fontWeight: 300,
      },
    },
    dctx: {
      marginTop: "20px",
      width: "100%",
    },
  }
})

export default LandingStyle
