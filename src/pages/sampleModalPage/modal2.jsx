import { Link, useModal } from "../../components/modal"

import React from "react"

export default () => {
  const [modal, closeTo] = useModal()

  return (
    <div>
      <Link to={closeTo}>Close</Link>
      <h1>Sample Modal Page 2</h1>
      <Link to="/sampleModalPage" asModal replace>
        Go to Sample Modal 1
      </Link>
    </div>
  )
}
