import { Link, useModal } from "../../components/modal"

import React from "react"
import myStyle from "./__style"

/* Using useModal hook means expecting this page as a modal.
    "replace" prop of "Link" component is a valid prop of React Router.
    Please see link for reference: https://reacttraining.com/react-router/web/api/Link
*/
export default () => {
  const [modal, closeTo] = useModal(),
    classes = myStyle()

  return (
    <div className={classes.content}>
      <Link to={closeTo}>Close</Link>
      <h1>Sample Modal Page</h1>
      <Link to="/sampleModalPage/modal2" asModal replace>
        Go to Sample Modal 2
      </Link>
    </div>
  )
}
